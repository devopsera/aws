resource "aws_vpc" "this" {
  count = var.create_vpc ? 1 : 0

  cidr_block       = var.vpc_cidr
  instance_tenancy = var.instance_tenancy

  enable_dns_support   = var.dns_support
  enable_dns_hostnames = var.dns_hostnames

  tags = merge(
    {
      "Name" = format("%s", var.name)
    },
    var.tags,
  )

} 
