variable "create_vpc" {
  description = "Controls if VPC should be created (it affects almost all resources)"
  type        = bool
  default     = true
}

variable "name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
  default     = ""
}

variable "vpc_cidr" {
  type        = string
  default     = ""
  description = "The default cidr block for VPC"
}

variable "instance_tenancy" {
  description = "A tenancy option for instances launched into the VPC"
  type        = string
  default     = "default"
}

variable "tags" {
  description = "Default tags to set on the VPC."
  type        = map
  default = {
   Name = "defaultVPC"
  }
}

variable "dns_support" {
  description = "Enable or disable the dns support for vpc"
  type        = bool
  default     = true
}

variable "dns_hostnames" {
  description = "Enable or disable the dns hostname for vpc"
  type        = bool
  default     = false
}

variable "vpc_id" {
  type        = string
  default     = ""
  description = "The VPC ID for an existingn VPC"
}
