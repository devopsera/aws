# The `vpc` module.

This module will allow us to create new `vpc`.


## Requirements
| Name | Version |
| ---------- | --------- |
| terraform  | >=1.1.0   |

## Providers
| Name | Version |
| ---------- | --------- |
| aws        | >=4.26.0  |

## Resources

| Name | Type |
| ---------- | --------- |
| aws_vpc | resource |

## Inputs
| Name | Description | Type |
| ---------- | --------- | ---------- |
| vpc_cidr_block | The `ipV4 vpc_cidr_block` for the vpc | `string` |
| instance_tenancy | The `instance_tenancy` property for the vpc | `string` |
| dns_support  | To Enable or disable the dns support for vpc | `bool` |
| tags | A map of `tags` for your `vpc` | `map` |

## Outputs

| Name | Description |
| ---------- | --------- |
| vpc_id | The `vpc_id` of the vpc created. |
